<?php

namespace Varhall\Filino\Handlers;

use Nette\Http\FileUpload;
use Nette\Utils\Image;
use Varhall\Filino\Models\FileVersion;

class ImageHandler extends AbstractHandler
{
    protected $imageSizes = [
        [ 'type' => 'original' ],
        [ 'width' =>  1280, 'height' => 720, 'type' => 'large', 'primary' => TRUE ],
        [ 'width' =>  640,  'height' => 360, 'type' => 'medium' ],
        //[ 'width' =>  720,  'height' => 720, 'type' => 'large_rect', 'resize' => Image::EXACT ],
        //[ 'width' =>  360,  'height' => 360, 'type' => 'medium_rect', 'resize' => Image::EXACT ],
    ];

    public function mimeTypes()
    {
        return [
            'image/*'
        ];
    }

    public function save(FileUpload $file, $namespace = NULL)
    {
        if (!$file->isImage())
            throw new InvalidArgumentException('Given file is not image');

        $filename = $this->uniqueFileName();
        $versions = [];

        foreach ($this->imageSizes as $size) {
            $image = $file->toImage();

            if (isset($size['width']) && isset($size['height']))
                $image->resize($size['width'], $size['height'], isset($size['resize']) ? $size['resize'] : Image::FIT | Image::SHRINK_ONLY);

            $imgname = $this->imageName($file, $filename, $size);
            $fullPath = $this->buildPath([$this->absoluteStoragePath($namespace), $imgname]);
            $image->save($fullPath);

            $versions[] = FileVersion::instance([
                'path'          => $this->buildPath([$this->relativeStoragePath($namespace), $imgname]),
                'size'          => (new \SplFileInfo($fullPath))->getSize(),
                'mime_type'     => $file->getContentType(),
                'type'          => $size['type'],
                'primary'       => isset($size['primary']) ? $size['primary'] : FALSE,
                'attributes'    => [
                    'width'         => $image->getWidth(),
                    'height'        => $image->getHeight(),
                ]
            ]);
        }

        return $versions;
    }

    protected function imageName(FileUpload $file, $filename, array $size)
    {
        $suffix = '';

        if (isset($size['width']) && isset($size['height']))
            $suffix = "_{$size['width']}x{$size['height']}";

        return $filename . $suffix . '.' .  pathinfo($file->getName(), PATHINFO_EXTENSION);
    }
}