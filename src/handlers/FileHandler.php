<?php

namespace Varhall\Filino\Handlers;

use Nette\Http\FileUpload;
use Varhall\Filino\Models\FileVersion;

class FileHandler extends AbstractHandler
{
    public function mimeTypes()
    {
        return [];
    }

    public function save(FileUpload $file, $namespace = NULL)
    {
        $filename = $this->uniqueFileName() . '.' . pathinfo($file->getName(), PATHINFO_EXTENSION);

        file_put_contents($this->buildPath([$this->absoluteStoragePath($namespace), $filename]), $file->getContents());

        return [
            FileVersion::instance([
                'path'          => $this->buildPath([ $this->relativeStoragePath($namespace), $filename ]),
                'size'          => $file->size,
                'mime_type'     => $file->getContentType(),
                'type'          => 'original',
                'primary'       => TRUE,
                'attributes'    => NULL
            ])
        ];
    }
}