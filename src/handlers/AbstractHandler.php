<?php

namespace Varhall\Filino\Handlers;

use Nette\Http\FileUpload;
use Varhall\Filino\Models\File;

abstract class AbstractHandler
{
    const SEPARATOR = '/';

    public abstract function mimeTypes();

    public abstract function save(FileUpload $file, $namespace = NULL);

    protected function relativeStoragePath($namespace)
    {
        return $this->buildPath([ $namespace, date('Y'), date('m') ]);
    }

    protected function absoluteStoragePath($namespace, $create = TRUE)
    {
        $directory = $this->buildPath([ File::getBasePath(), $this->relativeStoragePath($namespace) ]);

        if ($create && !file_exists($directory))
            mkdir($directory, 0777, TRUE);

        return $directory;
    }

    protected function buildPath(array $parts)
    {
        return implode(self::SEPARATOR, $parts);
    }

    protected function uniqueFileName()
    {
        return date('YmdHis') . rand(100, 999);
    }
}