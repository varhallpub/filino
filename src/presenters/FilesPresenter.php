<?php

namespace Varhall\Filino\Presenters;

use Nette\Application\Responses\FileResponse;
use Nette\Application\Responses\TextResponse;
use Nette\Http\Response;
use Varhall\Filino\Models\File;
use Varhall\Filino\Models\FileVersion;

trait FilesPresenter
{
    /////////////////////////////////////////// API METHODS ////////////////////////////////////////////////////////////

    public function renderUpload(array $data, array $files)
    {
        throw new \Nette\NotSupportedException('Upload method is not supported');
    }

    public function renderDownload($id, $version = NULL)
    {
        $version = str_replace('-', '_', $version);

        $obj = $version ? File::find($id)->version($version) : File::find($id)->primary();

        if ($obj && $obj instanceof FileVersion)
            return $this->sendFile($obj);

        $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
        $this->sendResponse(new TextResponse('File not found'));
    }


    ////////////////////////////////////////// HELPER METHODS //////////////////////////////////////////////////////////

    protected function sendFile(FileVersion $file, $name = NULL)
    {
        $response = new FileResponse(
            $file->read(),
            !empty($name) ? $name : $file->file()->name,
            $file->mime_type,
            FALSE
        );

        $this->sendResponse($response);
    }

    protected function sendFileNotFound()
    {
        $this->sendResponse(new TextResponse('Requested file does not exist', Response::S404_NOT_FOUND));
    }
}