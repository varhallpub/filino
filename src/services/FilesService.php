<?php

namespace Varhall\Filino\Services;

use Varhall\Filino\Handlers\AbstractHandler;
use Varhall\Filino\Handlers\FileHandler;
use Varhall\Filino\Handlers\ImageHandler;
use Varhall\Filino\Models\File;

/**
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class FilesService
{
    protected $handlers = [];

    public function __construct()
    {
        $this->addHandler(new ImageHandler());
    }

    public function addHandler(AbstractHandler $handler)
    {
        $this->handlers[] = $handler;
    }



    public function saveFile(\Nette\Http\FileUpload $file, $namespace = NULL)
    {
        $handler = $this->findHandler($file->getContentType());

        $versions = $handler->save($file, $namespace);

        // save file
        $fileObject = File::create([
            'name'          => $file->name,
            'namespace'     => $namespace
        ]);
        
        // save versions
        foreach ($versions as $version) {
            $version->file_id = $fileObject->id;
            $version->save();
        }
        
        return $fileObject;
    }
    
    // PROTECTED METHODS //

    protected function findHandler($mimeType)
    {
        foreach ($this->handlers as $handler) {
            foreach ($handler->mimeTypes() as $mt) {
                $pattern = '^' . str_replace('*', '.*', $mt) . '$';

                if (preg_match("#{$pattern}#i", $mimeType))
                    return $handler;
            }
        }

        return new FileHandler();
    }
}