<?php

namespace Varhall\Filino\DI;

use Nette\DI\Config\Helpers;

/**
 * Nette extension class
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class FilinoExtension extends \Nette\DI\CompilerExtension
{
    protected function configuration()
    {
        $builder = $this->getContainerBuilder();
        return Helpers::merge($this->getConfig(), [
            'storage'      => $builder->parameters['wwwDir'] . DIRECTORY_SEPARATOR . 'files',
        ]);
    }

    /**
     * Processes configuration data
     *
     * @return void
     */
    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('filino'))
            ->setFactory('Varhall\Filino\Services\FilesService');
    }

    public function afterCompile(\Nette\PhpGenerator\ClassType $class)
    {
        parent::afterCompile($class);

        $configuration = $this->configuration();

        // metoda initialize
        $initialize = $class->getMethod('initialize');

        $initialize->addBody('\Varhall\Filino\Models\File::setBasePath(?);', [ $configuration['storage'] ]);
    }
}
