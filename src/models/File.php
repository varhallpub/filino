<?php

namespace Varhall\Filino\Models;

use Varhall\Dbino\Model;
use Varhall\Dbino\Plugins\JsonPlugin;
use Varhall\Dbino\Plugins\TimestampPlugin;

class File extends Model
{
    protected static $basePath = '';

    public static function getBasePath()
    {
        return self::$basePath;
    }

    public static function setBasePath($basePath)
    {
        self::$basePath = $basePath;
    }

    public function duplicate(array $values = [], array $except = [])
    {
        $clone = parent::duplicate($values, $except);

        foreach ($this->versions() as $version) {
            $version->duplicate([ 'file_id' => $clone->id ]);
        }

        return $clone;
    }

    ////////////////////////////////////// RELATIONS ///////////////////////////////////////////////////////////////////

    public function versions()
    {
        return $this->hasMany(FileVersion::class, 'file_id');
    }

    public function version($type)
    {
        return $this->versions()->where('type', $type)->fetch();
    }

    public function primary()
    {
        return $this->versions()->where('primary', TRUE)->fetch();
    }

    ///////////////////////////////////// CONFIGURATION ////////////////////////////////////////////////////////////////

    protected function plugins()
    {
        return [
            new TimestampPlugin(),
            new JsonPlugin(['attributes'])
        ];
    }

    protected function table()
    {
        return 'files';
    }
}