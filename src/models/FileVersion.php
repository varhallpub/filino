<?php

namespace Varhall\Filino\Models;

use Nette\Utils\FileSystem;
use Varhall\Dbino\Model;
use Varhall\Dbino\Plugins\JsonPlugin;
use Varhall\Dbino\Plugins\TimestampPlugin;

class FileVersion extends Model
{
    public function read()
    {
        $filename = File::getBasePath() . DIRECTORY_SEPARATOR . $this->path;

        if (!file_exists($filename))
            throw new \InvalidArgumentException('Given file does not exist');

        return new \SplFileInfo($filename);
    }

    public function duplicate(array $values = [], array $except = [])
    {
        $file = $this->read();

        $name = preg_replace('/^c[0-9]+_/i', '', $file->getFilename());

        // clone db record
        $clone = parent::duplicate($values, $except);

        // clone file
        $cloneFileName = "c{$clone->id}_{$name}";
        FileSystem::copy($file->getRealPath(), $file->getPath() . DIRECTORY_SEPARATOR . $cloneFileName, FALSE);

        // update path in db
        $clone->update([
            'path'  => str_replace($file->getFilename(), $cloneFileName, $clone->path)
        ]);

        return $clone;
    }

    ////////////////////////////////////// RELATIONS ///////////////////////////////////////////////////////////////////

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    ///////////////////////////////////// CONFIGURATION ////////////////////////////////////////////////////////////////

    protected function plugins()
    {
        return [
            new TimestampPlugin(),
            new JsonPlugin(['attributes'])
        ];
    }

    protected function table()
    {
        return 'file_versions';
    }
}